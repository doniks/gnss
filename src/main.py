import time
import os

import pycom
import machine
import binascii

from log import *
from pycoproc_2 import Pycoproc
from Quectel_L76 import Quectel_L76

##############
# config
##############
action = 'decode'
# action = 'print'
# action = 'log'

# background = False
background = True

##############
# functions
##############
def sleep(s, verbose=False):
    while s:
        verbose and print('.', end='')
        time.sleep(1)
        s -= 1
    verbose and print()

def _fix_boot_config():
    boot_configs = [pycom.heartbeat_on_boot, pycom.lte_modem_en_on_boot, pycom.pybytes_on_boot, pycom.smart_config_on_boot, pycom.wifi_on_boot]

    for b in boot_configs:
        if b():
            print('boot_configs not ok')
            for bb in boot_configs:
                bb(False)
            print('fixed, rebooting')
            time.sleep(0.1)
            machine.reset()
    print('boot_configs ok')

def stop():
    try:
        gnss.stop()
        print('gnss stopped')
        sleep(2, True)
    except Exception as e:
        # gnss isn't defined
        # that's normal!
        # print(e)
        pass

def start():
    global gnss, nmea, tracker
    stop()
    print('start')
    gnss = Quectel_L76()
    print(gnss)
    if action == 'decode':
        from NMEA import NMEA
        nmea = NMEA()
        nmea._parser_default = print
        gnss.nmea_parse_callback(nmea.parse)

        def my_rmc_cbk():
            # print only once per minute
            if nmea._ts[2] == 0:
                nmea.print_overview()

        # nmea.rmc_callback(my_rmc_cbk)
    elif action == 'log':
        import sys
        log_rotate(400_000)

        gnss._sleep_after_newline_s = 1
        gnss.set_msg_freq(5)

        from NMEA import NMEA
        nmea = NMEA()

        from Tracker import Tracker
        tracker = Tracker(nmea, track_file='/flash/track.csv')
        nmea.fix_callback(tracker.fix_callback)

        nmea.print_stats_header()
        # nmea._print_sat_stats = True
        # nmea.rmc_callback(nmea.print_stats)

        def nmea_callback(s):
            nmea.parse(s)
            # log(n)
            log(s, quiet=True)
        gnss.nmea_parse_callback(nmea_callback)

        if False:
            log_tail()
            log_rm()
            log_rm(all=True)
            log_size()

    else:
    #     print('Doing nothing (action={})'.format(action))
    # elif action == 'print':
        # "nothing" is the same as print :-P
        pass

    if background:
        import _thread
        _thread.start_new_thread(gnss.run, ())
        time.sleep(0.1)
        print('isrunning', gnss.isrunning())

        if action == 'log':
            time.sleep(1)
            log_tail()
            log_size()
    else:
        gnss.run()

def off():
    print('entering eternal deepsleep (until reset button/power cycle)')
    time.sleep(.2)
    import machine
    machine.deepsleep() # sleep until reset button

def w(s):
    gnss.write_nmea(s)

def reset():
    py.setup_sleep(10) # 10s
    py.setup_sleep(3600*24) # 24h
    py.go_to_sleep()
    py.reset_cmd()

def delete():
    print('delete everything')
    stop()
    sleep(3, True) # give a chance for panic Ctrl-C
    log_rm(all=True)
    os.remove('/flash/track.log')
    os.remove('/flash/track.csv')
    log_size()

def status():
    # print(gnss._nmea)
    print('isrunning', gnss.isrunning())
    print("nmea._parser_failures", nmea._parser_failures)
    print('gnss._count z/nl/ok/fail/ovrfl', gnss._count_zero, gnss._count_newline, gnss._count_checksum_ok, gnss._count_checksum_failed, gnss._count_overflow)

    # log_tail()
    # log_size()
    #
    # print(tracker.ttff_s, tracker.lat, tracker.lon, tracker.total_km)
    # # tracker.verbose = True
    # # tracker.verbose = False
    # from shell import *
    # tail('/flash/track.log')

##############
# main
##############
pycom.heartbeat(False)
pycom.rgbled(0x000202)
uid = binascii.hexlify(machine.unique_id())
name = os.uname().sysname.lower() + '-' + uid.decode("utf-8")[-4:]
print(name)

if True:
    _fix_boot_config()

    py = Pycoproc()
    print('pycoproc', hex(py.read_product_id()))
    if py.read_product_id() != Pycoproc.USB_PID_PYTRACK:
        raise Exception('Not a Pytrack')

    print("Battery: {:4.2f} V".format(py.read_battery_voltage()))

    print(time.time(), action, background)
    sleep(3, True)

start()

if False:
    status()
    stop()
    off()
    reset()
    delete()
    w('PSTMSTAGPS8PASSGEN')
    w('PSTMDUMPEPHEMS')
