#!/usr/bin/env python3
# NMEA handler
# parses NMEA sentences and collects the GNSS data in convenient form
#

import time
import sys

from pstm_par import print_pstm_par
from gps_ephem import Ephem

ephe_tags = ['week','toe','toc','available','health','accuracy']


class NMEA:

    # callbacks for nmea sentence parsers
    _parser = { }

    SYSTEM_GPS     = 1
    SYSTEM_GLONASS = 2
    SYSTEM_GALILEO = 3
    SYSTEM_BEIDOU  = 4

    SYSTEM_NAME = {
        SYSTEM_GPS    : 'GP',
        SYSTEM_GLONASS: 'GL',
        SYSTEM_GALILEO: 'GA',
        SYSTEM_BEIDOU : 'BD',
        # GN is used when a combination of systems are at play
    }

    def __init__(self, rmc_callback=None, fix_callback=None):
        self._parser['RMC'] = self._parse_rmc
        self._parser['GSA'] = self._parse_gsa
        self._parser['GSV'] = self._parse_gsv
        self._parser['GGA'] = self._parse_gga
        self._parser['GST'] = self._parse_gst

        self._parser['GLL'] = self._ignore
        self._parser['VTG'] = self._ignore
        self._parser_default = print
        # self._parser_default = self._ignore

        # proprietary Quectel sentences
        self._parser['EPE'] = self._parse_pqepe      # PQEPE

        # proprietary STM sentences
        self._parser['TMUTC'] = self._ignore
        self._parser['TMCPU'] = self._ignore
        self._parser['TMALMANAC'] = self._parse_alma
        self._parser['TMEPHEM'] = self._parse_ephe
        self._parser['TMSETPAR'] = self._parse_stm_setpar
        self._parser_failures = 0

        self._rmc_callback = rmc_callback
        self._fix_callback = fix_callback

        # hh, mm, ss, ssss
        self._hhmmss = [None, None, None]
        # yyyyy, mm, dd
        self._yyyymmdd = [None, None, None]
        self._valid = False
        self._pos_mode = None
        self._fix_mode = None
        self._gsa_fix_status = None
        self._gga_status = None
        self._lat = None
        self._lon = None
        self._alt = None
        self._pdop = None
        self._hdop = None
        self._vdop = None
        # number of satellites in view per talker
        self._sats_view = {}
        # number of satellites used
        self._sats_used = None
        self._dgps = None

        # keep for each satellite:
        # (last_seen_time_s, azi, ele, snr, )
        self._sats = {}
        # self._ids = []
        # # prefill de dict with some known satellites, that makes it a bit easier to work with the logfile
        # self._ids = ['02', '03', '04', '05', '06', '07', '08', '10', '11', '12', '18', '20', '23', '24', '25', '26', '29', '30', '31', '32', '33', '37', '39', '40', '65', '66', '67', '68', '70', '71', '72', '73', '75', '76', '79', '80', '81', '82', '83', '85', '88']
        # # 67  66  71  88  85  75  68  06  81  11  12  82  80  25  20  33  79  37  30  40  02  29  32  26  23  39  10
        # # GL  GL  GL  GL  GL  GL  GL  GP  GL  GP  GP  GL  GL  GP  GP  GP  GL  GP  GP  GP  GP  GP GP  GP GP  GP GP
        # for id in self._ids:
        #     self._sats[id] = {}
        #     self._sats[id]['tlk'] = '  '
        #     self._sats[id]['snr'] = None
        # average snr
        self._snr = None

        self._ehpe = None
        self._latErr = None
        self._lonErr = None
        self._altErr = None

        self._epe_h = None
        self._epe_v = None

        self._print_sat_stats = False
        self._print_last_sats_len = 0

    def _sat(self, id):
        if isinstance(id, str):
            id = int(id)
        if id not in self._sats:
            self._sats[id] = {}
        return self._sats[id]

    def dt(self):
        import datetime
        try:
            return datetime.datetime(self._yyyymmdd[0], self._yyyymmdd[1], self._yyyymmdd[2], self._hhmmss[0], self._hhmmss[1], self._hhmmss[2])
        except:
            return None

    def ts(self):
        import datetime
        try:
            timedelta_since_epoch = self.dt()-datetime.datetime(1970,1,1)
            return int(round((timedelta_since_epoch).total_seconds()))
        except:
            return None

    def rmc_callback(self, callback):
        self._rmc_callback = callback

    def fix_callback(self, callback):
        self._fix_callback = callback

    def _checksum(self, nmea):
        calc_cksum = 0
        for s in nmea:
            calc_cksum ^= ord(s)
        return('{:02X}'.format(calc_cksum))

    def parse(self, nmea):
        try:
            data, checksum = nmea.split('*')
        except Exception as e:
            self._parser_failures += 1
            # print('Failed to split nmea: ({}:{}) "{}"'.format(type(e), e, nmea))
            return
        calc_checksum = self._checksum(data[1:])
        if checksum != calc_checksum:
            self._parser_failures += 1
            print('failed checksum "{}"'.format(nmea), '"{}" != "{}"'.format(checksum, calc_checksum))
            return
        tokens = data.split(',')
        cmd = tokens[0][3:]
        parse_callback = self._parser.get(cmd)
        # print(cmd, parse_callback)
        if parse_callback is None:
            if self._parser_default:
                self._parser_default(tokens)
            # print('skip', tokens)
            pass
        else:
            try:
                parse_callback(tokens)
            except Exception as e:
                self._parser_failures += 1
                print('Failed to parse ({}:{}) \'{}\''.format(type(e), e, nmea), self._checksum(data), tokens, parse_callback)
        return tokens

    def _ignore(self, tokens):
        pass

    def _parse_time(self, token):
        hms, sss = token.split('.')
        return (int(hms[0:2]), int(hms[2:4]), int(hms[4:6]), int(sss))

    def _parse_lat(self, latitude, ns):
        """
        Parameters
        ----------
        latitude : str
            Latitude in format "ddmm.mmmm" (degrees and minutes)
        ns : str
            N=North, or S=South
        """
        if len(latitude) == 0:
            return
        if len(ns) == 0:
            return
        # print('lat', latitude, ns)
        d = int(latitude[0:2])
        m = float(latitude[2:])
        deg = d + m / 60
        if ns == "N":
            return deg
        else:
            return - deg

    def _parse_lon(self, longitude, ew):
        """
        Parameters
        ----------
        longitude : str
            Longitude in format "ddmm.mmmm" (degrees and minutes)
        ew : str
            E=East, W=West
        """
        # print('lon', longitude, ew)
        if len(longitude) == 0:
            return
        if len(ew) == 0:
            return
        i,f = longitude.split('.')
        d = int(i[0:-2])
        m = float(i[-2:] + '.' + f)
        deg = d + m / 60
        # print(deg)
        if ew == "E":
            return deg
        else:
            return - deg

    def _parse_date(self, ddmmyy):
        """
        Parameters
        ----------
        date : str
            Date in format "ddmmyy"
        """
        return (int("20" + ddmmyy[4:6]), int(ddmmyy[2:4]), int(ddmmyy[0:2]))

    def _parse_rmc(self, tokens):
        # print('rmc', tokens)
        # ['$GNRMC', '142835.000', 'A', '5140.7483', 'N', '00519.0891', 'E', '0.00', '250.73', '041221', '', '', 'D', 'V']
        # --RMC, timestamp, AV, lat, NS, lon, EW, speed[knots], course over ground, date, NA, NA, Positioning Mode, „V‟=Navigational status not valid

        self._hhmmss = self._parse_time(tokens[1])
        self._valid = tokens[2] == 'A' # V is invalid
        self._lat = self._parse_lat(tokens[3], tokens[4])
        self._lon = self._parse_lon(tokens[5], tokens[6])
        self._yyyymmdd = self._parse_date(tokens[9])

        # Positioning Mode:
        # „N‟=No fix
        # „A‟=Autonomous GNSS fix
        # „D‟=Differential GNSS fix
        self._pos_mode = tokens[12]

        if self._rmc_callback:
            self._rmc_callback()
        if self._fix_callback and self._valid:
            self._fix_callback()

    def _parse_gga(self, tokens):
        # print('gga', tokens)
        # ['$GPGGA', '171721.000', '5140.7624', 'N', '00519.0085', 'E', '2', '14', '0.72', '22.6', 'M', '47.3', 'M', '', '']
        # timestamp, lat, ns, lon, ew, no/gnss/dgps/est, satellites used, hdop,  altitude, M, height of geoid, M, dgps age, dgps station
        s = tokens[6]
        if s == '0':
            self._gga_status = 'Inv'
        elif s == '1':
            self._gga_status = 'GNSS'
        elif s == '2':
            self._gga_status = 'DGPS'
        elif s == '6':
            self._gga_status = 'Est'
        else:
            self._gga_status = '?{}'.format(s)
        self._sats_used = int(tokens[7])
        # print("hdop:", tokens[8], self._hdop)
        if len(tokens[9]) > 0:
            self._alt = float(tokens[9])
        else:
            self._alt = None
        self._dgps = tokens[13:15]

    def _parse_gsa(self, tokens):
        # print('gsa', tokens) # DOP and active satellites
        # ['$GNGSA', 'A', '3', '15', '17', '06', '24', '19', '12', '10', '', '', '', '', '', '1.23', '0.72', '1.00', '1']
        # ['$GNGSA', 'A', '3', '88', '78', '69', '68', '87', '86', '77', '', '', '', '', '', '1.23', '0.72', '1.00', '2']
        # GNGSA, mode=M(anual)/A(auto switch 2D/3D), Fix=1No/2D/3D, 12 satellites, pdop, hdop, vdop, system

        # Teseo Liv3F: talker ID is always 'GN', no system ID tokens[18], but it seems all satellite id's are unique
        # $GNGSA,A,3,73,82,83,84,74,,,,,,,,1.3,0.8,1.1*2C
        # $GNGSA,A,3,313,315,327,,,,,,,,,,1.3,0.8,1.1*16
        # $GNGSA,A,3,25,02,06,12,31,,,,,,,,1.3,0.8,1.1*24
        # $GNGSA,A,3,73,82,83,84,66,,,,,,,,1.3,0.8,1.1*2F
        #
        # $GNGSA,A,3,313,315,327,,,,,,,,,,1.3,0.8,1.1*16
        # $GNGSA,A,3,25,02,06,12,,,,,,,,,1.3,0.8,1.1*26
        # $GNGSA,A,3,73,82,83,84,,,,,,,,,1.3,0.8,1.1*2F

        # I think we can have multiple for all the systems:
        # GNSS System ID (Only supported for NMEA V4.10)
        # 1 - GPS
        # 2 - GLONASS Quectel
        # 3 - Galileo
        # 4 - BeiDou
        # but I guess fix_mode, _gsa_fix_status and dop are always the same
        now = self.ts()
        self._fix_mode = tokens[1]
        s = tokens[2]
        if s == '1':
            self._gsa_fix_status = 'No'
        elif s == '2':
            self._gsa_fix_status = '2D'
        elif s == '3':
            self._gsa_fix_status = '3D'
        else:
            print('Failed to parse GSA:', tokens)
            return
        for sid in tokens[3:15]:
            if len(sid) > 0:
                self._sat(sid)['gsa-ts'] = now
        self._pdop = tokens[15]
        self._hdop = tokens[16]
        self._vdop = tokens[17]

    def _parse_gsv(self, t):
        # print('gsv', t)
        # ['$GPGSV', '3', '1',  '11',  '12', '71', '238', '17',  '24', '69', '127', '46',  '19', '35', '055', '46',  '25', '35', '245', '27',  '0']
        # ['$GPGSV', '3', '2',  '11',  '32', '30', '309', '20',  '17', '16', '038', '42',  '40', '15', '123', '31',  '06', '12', '089', '47',  '0']
        # ['$GPGSV', '3', '3',  '11',  '10', '11', '266',   '',  '15', '11', '176', '31',  '02', '02', '128', '40',                            '0']
        # ['$GLGSV', '2', '1',  '08',  '78', '65', '047', '47',  '69', '45', '271', '21',  '87', '35', '053', '48',  '88', '33', '108', '48',  '1']
        # ['$GLGSV', '2', '2',  '08',  '68', '22', '213', '22',  '70', '17', '337',   '',  '77', '14', '080', '40',  '81', '05', '152', '45',  '1']
        # ['$GAGSV', '1', '1', '01', '311', '00', '000', '14', '', '', '', '', '', '', '', '', '', '', '', '']
        # GSV, messages, message, satellites in view, ID1, Elev1, Azim1, SNR1, .... SNR4, Signal ID
        now = self.ts()
        talker = t[0][1:3]
        self._sats_view[talker] = int(t[3])
        # FIXME: what if we previously had glonass and gps and then we lose all gps satellites. nobody will reset the _sats_view
        idx = 4 # index of first sat id
        for s in range(4): # four satellites per sentence
            id = t[idx]
            if len(id) == 0:
                # no more satellites
                return
            # if id not in self._sats:
            #     self._sats[id] = {}
            #     # self._ids.append(id)
            #     # self._ids.sort()
            sat = self._sat(id)
            sat['gsv-ts'] = now
            sat['tlk'] = talker
            sat['ele'] = int(t[idx+1])
            sat['azi'] = int(t[idx+2])
            # Signal to noise ration in dBHz (0~99), empty if not tracking
            try:
                sat['snr'] = int(t[idx+3])
            except:
                sat['snr'] = None
            idx += 4

    def _parse_gst(self, tokens):
        # print('gst', tokens)
        # $GPGST,154225.000,0.0,0.0,0.0,-0.0,0.0,0.0,0.0*4F
        # $GPGST,113102.000,30.5,23.5,15.6,-0.8,20.0,19.9,17.3*44
        # $GPGST,113052.000,91.5,61.4,58.1,0.1,61.4,58.1,51.6*6D
        # $<TalkerID>GST,<Timestamp>,<EHPE>,<Semi-major>,<Semi-minor>,<Angle>,<LatErr>,<LonErr>,<AltErr Dev>*<checksum><cr><lf>

        self._ehpe = tokens[2]
        # self._semi_major = tokens[3]
        # self._semi_minor = tokens[4]
        # self._angle
        self._latErr = tokens[6]
        self._lonErr = tokens[7]
        self._altErr = tokens[8]

    def _parse_alma(self, tokens):
        # print('alma', tokens)
        # $PSTMALMANAC,<sat_id>,<N>,<byte1>,...,<byteN>*<checksum><cr><lf>
        sat = self._sat(tokens[1])
        sat['alma-ts'] = self.ts()
        sat['alma-l'] = tokens[2]
        sat['alma'] = tokens[3]

    def _parse_ephe(self, tokens):
        # print('ephe', tokens)
        # $PSTMEPHEM,<sat_id>,<N>,<byte1>,...,<byteN>*<checksum><cr><lf>
        # $PSTMEPHEM,1,64,0f06bc34bc345f5f5f84f400dea4ff00f9f63c239f0a35f81400fbff33420000ee632f27698ef

        sat = self._sat(tokens[1])
        sat['ephe-ts'] = self.ts()
        sat['ephe-l'] = tokens[2] # looks like GP/GL is 64, GA is 68
        # sat['ephe'] = tokens[3]
        try:
            if sat.get('tlk', None) == 'GP':
                e = Ephem(tokens[3])
                e.print()
                for t in ephe_tags:
                    sat['E-' + t] = e.get(t)
            else:
                sat['ephe-x'] = 'noGP'
        except Exception as ex:
            sat['ephe-x'] = 'EX'
            print(ex)

    def _parse_stm_setpar(self,tokens):
        # $PSTMSETPAR,1200,0x19639a5c*53
        # cfg = tokens[1][0]
        par = tokens[1][1:4]
        val = int(tokens[2], 16)
        print()
        print_pstm_par(par, val, verbose=True)
        print()

    def _parse_pqepe(self, tokens):
        # print('pqepe', tokens)
        # $PQEPE,0.753045,0.530208
        # <EPE_hori> Estimated horizontal position error
        # <EPE_vert> Estimated vertical position error
        self._epe_h = tokens[1]
        self._epe_v = tokens[2]

    def print_overview_header(self):
        # print('Date Time Valid Pos Fix Gsa Gga Latitude   Longitude   Altitude   phv-dops used/view dgps')
        print('Date Time Valid ehpe err phv view use sbas')

    def print_overview(self):
        print(self.dt(), end=' ')
        print(self._valid, end=' ')
        # self._pos_mode, self._fix_mode, self._gsa_fix_status, self._gga_status, end=' ')
        # print('{:.6f},{:.6f},{:.1f}'.format(self._lat, self._lon, self._alt), end=' ')
        print(self._ehpe, 'err={}/{}/{}'.format(self._latErr, self._lonErr, self._altErr), end=' ')
        print('phv={}/{}/{}'.format(self._pdop, self._hdop, self._vdop), end=' ')

        now = self.ts()
        recent_sat_s = 5
        recent_eph_s = 3600 * 4 # 4 hours
        view = 0
        ephe = 0
        used = 0
        snr_sum = 0
        snr_cnt = 0
        sbas_snr_sum = 0
        sbas_snr_cnt = 0
        for id in self._sats:
            sat = self._sats[id]
            try:
                if now - sat['gsa-ts'] < recent_sat_s:
                    used += 1
            except:
                pass
            try:
                if now - sat['gsv-ts'] < recent_sat_s:
                    view += 1
                    snr = self._sats[id]['snr']
                    if snr is not None:
                        snr_sum += int(snr)
                        snr_cnt += 1
                        i = int(id)
                        if i >= 33 and i <= 64:
                            sbas_snr_sum += int(snr)
                            sbas_snr_cnt += 1
                    try:
                        if now - sat['ephe-ts'] < recent_eph_s:
                            ephe += 1
                    except:
                        pass
            except:
                pass
        if snr_cnt:
            snr = round(snr_sum / snr_cnt, 1)
        else:
            snr = None
        if sbas_snr_cnt:
            sbas_snr = round(sbas_snr_sum / sbas_snr_cnt, 1)
        else:
            sbas_snr = None
        view2 = 0
        for tlk in self._sats_view:
            view2 += self._sats_view[tlk]
        # print('v:{}/{}/{}'.format(view, view2, self._sats_view), end=' ')
        print('v:{}/{}/{}'.format(view, view2, ephe), end=' ')
        print('u:{}/{},{}'.format(self._sats_used, used, snr), end=' ')
        print('sbas:{},{}'.format(sbas_snr_cnt, sbas_snr), end=' ')
        #self._dgps)
        print()

    def print_stats_header(self):
        print('Time Date SNR Valid Latitude Longitude Altitude Sats_used Sats_in_view GP GL GA SBAS p-dop h-dop v-dop Ehpe latErr lonErr altErr epeh epev Pos Fix Gsa Gga dgps')

    def print_sbas(self):
        # it seems for sbas,
        # there is no almanac or ephemeris and it's not active in GSA
        # we do wee azimuth and elevation and SNR
        snr_sum = 0
        snr_cnt = 0
        for id in self._sats:
            sat = self._sats[id]
            i = int(id)
            if i >= 33 and i <= 64:
                snr = sat['snr']
                print(i, snr, sat)
                if snr is not None:
                    snr_cnt += 1
                    snr_sum += snr
        if snr_cnt > 0:
            snr = snr_sum/snr_cnt
            print('{} -> snr={:5.2f}'.format(snr_cnt, round(snr,2)))

    def guess_tlk_stm(id):
        if isinstance(id,str):
            id = int(id)
        if id < 33:
            return 'GP'
        elif id < 65:
            # according to datasheet param 200, sbas is 33 to 51
            return 'SBAS'
        elif id < 97:
            return 'GL'
        # elif id in [301, 302, 303, 304, 305, 307, 308, 309, 311, 313, 315, 316, 319, 321, 326, 327, 331, 333, 336]:
        #     # those id's I have seen from Liv3F in GAGSV
        #     return 'GA'
        # elif id in [312, 324, 325, 330]:
        #     # those id's I have seen from Liv3F in PSTMALMANAC and the are large
        #     return 'GA?'
        elif id >= 301 and id <= 336:
            return 'GA'
        else:
            return '?'

    def print_sats(self):
        now = self.ts()
        fmt = '{:>7} '
        tags = ['tlk', 'alma-l', 'alma-ts', 'ephe-l', 'ephe-ts']
        for t in ephe_tags:
            tags.append('E-' + t)
        tags += ['azi', 'ele', 'gsv-ts', 'snr', 'gsa-ts', ]
        print(fmt.format('id'), end='')
        print(fmt.format('guess'), end='')
        for t in tags:
            print(fmt.format(t), end='')
        print()
        for id in self._sats:
            sat = self._sats[id]
            print(fmt.format(id), end='')
            print(fmt.format(NMEA.guess_tlk_stm(id)), end='')
            for t in tags:
                v = sat.get(t, None)
                try:
                    if t.endswith('-ts'):
                        print(fmt.format(int(now-v)), end='')
                    else:
                        print(fmt.format(v), end='')
                except:
                    # print(v, end='')
                    print(fmt.format('-'), end='')
            print()

    def print_stats(self, do_print=True):
        # (print table header for individual snr)
        if self._print_sat_stats and do_print:
            if len(self._sats) != self._print_last_sats_len:
                print()
                print(self.dt(), end=' ')
                for id in self._sats:
                    print(id, end=' ')
                print()
                print(self.dt(), end=' ')
                for id in self._sats:
                    print(self._sats[id]['tlk'], end=' ')
                print()
                self._print_last_sats_len = len(self._sats)
                print(self.dt(), end=' ')
                print('-- ' * self._print_last_sats_len)

        do_print and print(self.dt(), end=' ')

        # determine average snr
        # (and print individual snr)
        snr_sum = 0
        snr_cnt = 0
        sbas = 0
        now = self.ts()
        for id in self._sats:
            sat = self._sats[id]
            # print(id, sat)
            i = int(id)
            if i >= 33 and i <= 64:
                if self._sats[id]['snr'] is not None:
                    sbas += 1
            try:
                snr = sat['snr']
                if snr is not None:
                    if now - sat['s'] > 10:
                        # print("forget", id)
                        snr = None
                        sat['snr'] = None
                    else:
                        snr_sum += int(snr)
                        snr_cnt += 1
                if snr is None:
                    snr = '  '
                if self._print_sat_stats and do_print:
                    print(snr, end=' ')
            except Exception as e:
                print(type(e), e, id, sat)


        if snr_cnt > 0:
            self._snr = snr_sum/snr_cnt
            do_print and print('   {:5.2f}'.format(round(self._snr,2)), end=' ')
        else:
            self._snr = None
            do_print and print('   None ', end=' ')

        if do_print:
            print(self._valid, self._lat, self._lon, self._alt, end=' ')
            view = 0
            for c in self._sats_view:
                view += self._sats_view[c]
            print('{} {} {} {} '.format(self._sats_used, view, self._sats_view, sbas), end=' ')
            print(self._pdop, self._hdop, self._vdop, self._ehpe, self._latErr, self._lonErr, self._altErr, end=' ')
            print(self._epe_h, self._epe_v, end=' ')
            print(self._pos_mode, self._fix_mode, self._gsa_fix_status, self._gga_status, self._dgps)

    def coords(self):
        if self._valid:
            return (self._lat, self._lon)
        else:
            return (None, None)

if __name__ == '__main__':
    try:
        print('try to run on pytrack')
        import pycom
        pycom.heartbeat(False)
        pycom.rgbled(0x000606)
        from Quectel_L76 import Quectel_L76
        gnss = Quectel_L76()
        nmea = NMEA()
        gnss.nmea_parse_callback(nmea.parse)

        # nmea.print_overview_header()
        # nmea.fix_callback(nmea.print_overview)

        # nmea._print_sat_stats = True
        nmea.print_stats_header()
        nmea.rmc_callback(nmea.print_stats)

        gnss.run()
    except Exception as e:
        print('failed to run on pytrack, try laptop ({})'.format(e))
        import sys
        import os

        nmea = NMEA()

        if len(sys.argv) > 1 and not os.path.isfile(sys.argv[1]):
            print('parse command line argument')
            # nmea.rmc_callback(nmea.print_stats)
            # nmea.print_overview()
            nmea.parse(sys.argv[1])
            nmea.print_sats()
            sys.exit()

        def my_print_sats():
            if nmea.ts() % 600 == 0: # 600 .. 10m
                # 10m
                nmea.print_overview()
            if nmea.ts() % 3600 == 0: # 3600 .. 1h
                # 1h
                nmea.print_sats()
                print()

        nmea.print_overview_header()
        # nmea.fix_callback(nmea.print_overview)
        # nmea.rmc_callback(nmea.print_overview)

        # nmea._print_sat_stats = True
        # nmea.print_stats_header()
        # nmea.rmc_callback(nmea.print_stats)

        # nmea._parser_default = nmea._ignore
        nmea.rmc_callback(my_print_sats)

        print('argv', len(sys.argv))
        if len(sys.argv) > 1:
            print('trying to read from file \'{}\''.format(sys.argv[1]))
            with open(sys.argv[1], errors='ignore') as f:
                for line in f:
                    nmea.parse(line.strip())
        else:
            print('trying to read from stdin')
            with open(sys.stdin.fileno(), errors='ignore', closefd=False) as f:
                for line in f:
                    nmea.parse(line.strip())
