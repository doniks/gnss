
GPS_EPHEM = [
#start, len, name, description
[None, 16, 'week',  'Week number of the Issue of Data'],
[None, 16, 'toe',   'Time of week for ephemeris epoch'],
[None, 16, 'toc',   'Time of week for clock epoch'],
[None,  8, 'iode1', 'Issue of data 1'],
[None,  8, 'iode2', 'Issue of data 2'],
[None, 10, 'iodc',  'Issue of data clock'],
[None, 14, 'i_dot', 'Rate of inclination angle'],
[None,  8, 'RESERVED', ''],
[None, 24, 'omega_dot', 'Rate of right ascension'],
[None,  8, 'RESERVED', 'Must be 0'],
[None, 16, 'crs', 'Amplitude of the sine harmonic correction to the orbit radius'],
[None, 16, 'crc', 'Amplitude of the cosine harmonic correction to the orbit radius'],
[None, 16, 'cus', 'Amplitude of the sine harmonic correction to the argument of latitude'],
[None, 16, 'cuc', 'Amplitude of the cosine harmonic correction to the argument of latitude'],
[None, 16, 'cis', 'Amplitude of the sine harmonic correction to the angle of inclination'],
[None, 16, 'cic', 'Amplitude of the cosine harmonic correction to the angle of inclination'],
[None, 16, 'motion_difference', 'Mean motion difference from computed value'],
[None, 16, 'RESERVED', 'Must be 0'],
[None, 32, 'inclination', 'Inclination angle at reference time'],
[None, 32, 'e',        'Eccentricity'],
[None, 32, 'root_A',   'Square root of major axis'],
[None, 32, 'mean_anomaly', 'Mean anomaly at reference time.'],
[None, 32, 'omega_zero', 'Longitude of ascending node of orbit plane at weekly epoch'],
[None, 32, 'perigee',  'Argument of perigee'],
[None,  8, 'time_group_delay', 'Estimated group delay differential'],
[None,  8, 'af2', 'Second order clock correction'],
[None, 16, 'af1', 'First order clock correction'],
[None, 22, 'af0', 'Constant clock correction'],
[None,  1, 'RESERVED',  'must be 1'],
[None,  1, 'RESERVED',  'must be 1'],
[None,  1, 'RESERVED',  'must be 1'],
[None,  1, 'available', 'Contains 1 if ephemeris is available, 0 if not'],
[None,  1, 'health',    'Contains 1 if the satellite is unhealthy, 0 if healthy'],
[None,  1, 'RESERVED',  'Must be 0'],
[None,  4, 'accuracy',  'Accuracy'],
]


GPS_EPHEM_LOOKUP = {
# name -> (start, len)
}
GPS_EPHEM_LEN = 0
for structure in GPS_EPHEM:
    structure[0] = GPS_EPHEM_LEN
    GPS_EPHEM_LEN += structure[1]

    s = structure[2]
    if s != 'RESERVERD':
        GPS_EPHEM_LOOKUP[s] = (structure[0], structure[1])
GPS_EPHEM_LEN = GPS_EPHEM_LEN//8
print(GPS_EPHEM_LEN)

# def decode_ephem(structures, ephemeris):
#     # ephemeris = b'\0xCA\0xFE'
#     for structure in structures:
#         bit_start = structure[0]
#         bit_len = structure[1]
#         byte_start = bit_start//8     # which byte does this structure start in
#         byte_bit_start = bit_start%8  # which bit inside byte_start does this structure start at
#         while True:
#             print(byte_start, '{:08b}/{:02X}'.format(ephemeris[byte_start], ephemeris[byte_start]), byte_bit_start, bit_len)
#             byte_bit_len = min(8-byte_bit_start, bit_len)
#             mask = 0
#             for b in range(8):

#         # byte_len = (byte_bit_start + bit_len)//8 # how many bytes does this structure end in

class Ephem:
    def __init__(self, ephemeris):
        # if len(ephemeris) != GPS_EPHEM_LEN:
        #     print('Invalid length', len(ephemeris))
        #     return
        self.bit_string = ""
        if isinstance(ephemeris, bytes):
            for b in ephemeris:
                self.bit_string += '{:08b}'.format(b)
        elif isinstance(ephemeris, str):
            for i in range(0, len(ephemeris), 2):
                self.bit_string += '{:08b}'.format(int(ephemeris[i:i+2],16))
        # print(self.bit_string)

    def get(self, structure):
        s = GPS_EPHEM_LOOKUP[structure]
        x = s[0]
        l = s[1]
        b = self.bit_string[x:x+l]
        v = int(b,2)
        # print(structure, x, l, b, v)
        # print('{} 0b{} {}'.format(structure, b, v))
        return v

    def print(self):
        for s in ['week', 'toe', 'toc', 'available', 'health', 'accuracy']:
            self.get(s)


if __name__ == '__main__':
    # decode_gps_ephem(b'\xCA\xFE')
    # decode_gps_ephem('CAFE')
    nmea = '$PSTMEPHEM,16,64,8c087b4f7b4f0202020000000000003a00000000000000000000000000000a003a0e9527797a640684fc0ca1da68f7d5b198dc8db77c1c1c0000cbff238ef10b*37'

    # nmea = '$PSTMEPHEM,89,64,8c086a5307800200210ceb013c8f2217e74d6505af548b00c161040684b328360104000035628002000000001e00000000000000000000003f0000000000000a*31'

    Ephem(nmea.split('*')[0].split(',')[3]).print()