# driver for Quectel L76 GNSS module
# connected via i2c
#

import time
import _thread
from machine import I2C

class Quectel_L76:

    I2C_ADDR = const(0x10)

    # callbacks for nmea sentence parsers
    _parser = { }

    def __init__(self, i2c=None, sda='P22', scl='P21', nmea_parse_callback=None, verbose=False):
        if i2c is not None:
            self._i2c = i2c
        else:
            self._i2c = I2C(0, mode=I2C.MASTER, pins=(sda, scl))
            verbose and print(self._i2c.scan())

        self._lock = _thread.allocate_lock()

        # buffer that is read from i2c
        self._buf = bytearray(128)
        # valid length in this buffer from last read
        self._buf_len = 0
        # next byte to be parsed
        self._buf_idx = 0

        # see analysis at the bottom
        self._sleep_after_newline_s = 2

        # stats counters
        self._count_newline = 0
        self._count_zero = 0
        self._count_overflow = 0
        self._count_checksum_ok = 0
        self._count_checksum_failed = 0

        self._verify_checksum = True

        # buffer where nmea sentences are constructed
        self._nmea = bytearray(256)
        self._clear(self._nmea)

        # callback for processing nmea sentences
        self._nmea_parse_callback = nmea_parse_callback

        # flags to handle background operation
        self._running = False
        self._stop = True
        self._verbose = verbose

        self.write_nmea('PMTK413') # query SBAS
        self.write_nmea('PMTK401') # query diff GNSS

        self.write_nmea('PQEPE,W,1,1') # enable

        # set message frequecies
        self.set_msg_freq(1)

        # set constellations
        # todo: if this is not the current configuration, it will lead to a reset, we should probably properly handle this, but for now, I guess it is sufficient to do it as last SET operation
        self.write_nmea('PMTK353,1,1,1,1,0') # GPS GLONASS GALILEO_FULL

    def set_msg_freq(self, freq):
        # self.write_nmea('PMTK314,0,1,0,1,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0') # RMC,GGA,GSA,GSV,GRS
        self.write_nmea('PMTK314,0,{0},0,{0},{0},{0},0,{0},0,0,0,0,0,0,0,0,0,0,0'.format(freq))

    def nmea_parse_callback(self, callback):
        self._nmea_parse_callback = callback

    def _read(self):
        for attempt in range(3):
            try:
                with self._lock:
                    self._buf_len = self._i2c.readfrom_into(I2C_ADDR, self._buf)
                self._buf_idx = 0
                # it seems a read is always 128 bytes
                # print('.', end='')
                # print('[{}]'.format(self._buf_len), end='')
                # print('READ[{}]({})'.format(self._buf_len, self._buf))
                return
            except Exception as e:
                # OSError: I2C bus error
                # this can happen temporarily right after boot
                print(attempt, e, self._buf_len, self._buf_idx)
                time.sleep(1+attempt)
        raise Exception('Unable to read from I2C bus {}'.format(e))

    def _pop(self):
        next = self._buf[self._buf_idx]
        self._buf_idx += 1
        if self._buf_idx >= self._buf_len:
            self._read()
        return next

    def _skip(self, b):
        """ skip all further bytes equal to b in the current _buf,
        then _read() a new buffer and
        return the number of skipped bytes """
        ct = 0
        next = self._buf[self._buf_idx]
        while next == b and not self._stop:
            ct += 1
            self._buf_idx += 1
            if self._buf_idx >= self._buf_len:
                self._read()
                break
            next = self._buf[self._buf_idx]
        return ct

    def _wait(self, b):
        """ wait until we get the byte b. read and ignore all other bytes until then"""
        ct = 0
        while not self._stop:
            next = self._buf[self._buf_idx]
            if next == b:
                return ct
            # print(next, end=' ')
            ct += 1
            self._pop()
        return ct

    def _clear(self, buf):
        for i in range(len(buf)):
            buf[i] = 0

    def read_nmea(self):
        # print('get_nmea')
        idx = 0
        self._clear(self._nmea)
        # self._skip(ord('\n'))
        while True:
            next = self._pop()
            if next == ord('\r'):
                next = self._pop()
                if next == ord('\n'):
                    # print('!', end='')
                    return self._nmea.decode()
                else:
                    print('<CR> without <NL>')
                    continue
                # print('CR')
            elif next == ord('\n'):
                # print('NL')
                # if idx > 0:
                #     print('N', end='')
                # else:
                #     print('_', end='')
                skipped_nl = 1
                skipped_nl += self._skip(ord('\n'))
                self._count_newline += skipped_nl
                # I've seen newlines in three situations:
                # - a single new line right after a carriage return - we handle these above
                # - two new lines at the end of self._buf. this can also happen in the middle of a sentence, so just skip the NL and carry on.
                # - longer sequences of new lines extending to the end of the buffer, sometimes the whole buffer is all newlines. In these cases I think that there wasn't any data to be read. ie., we're reading too fast
                if skipped_nl > 2:
                    # print('S({})'.format(skipped_nl), end='')
                    time.sleep(self._sleep_after_newline_s)
                continue
            # elif next == ord('$'):
            #     # print('DLR')
            elif next == 0:
                # print('ZERO')
                # print('z', end='')
                self._count_zero += 1
                continue
            else:
                pass
                # print(next, end=' ')
            if idx >= len(self._nmea):
                self._count_overflow += 1
                print('OVERFLOW')
                # this should not happen. either self._nmea is too short to fit the whole line, or we are somehow not detecting the end of the nmea sentence correctly. for now, we drop the current buffer and start over
                print('DROP[{}]({})'.format(idx, self._nmea))
                self._clear(self._nmea)
                idx = 0
            self._nmea[idx] = next
            idx += 1
            # print('NMEA[{}]({})'.format(idx, self._nmea))

    def _checksum(self, nmea):
        calc_cksum = 0
        for s in nmea:
            calc_cksum ^= ord(s)
        return('{:02X}'.format(calc_cksum))

    def write_nmea(self, nmea):
        self._verbose and print('write', nmea)
        with self._lock:
            self._i2c.writeto(I2C_ADDR, '${}*{}\r\n'.format(nmea, self._checksum(nmea)) )

    def run(self):
        if self._running:
            print('already running')
            return
        self._running = True
        self._stop = False
        self._wait(ord('$'))
        while not self._stop:
            nmea = self.read_nmea().rstrip('\x00')
            # print('[{:9.3f}] NMEA[{}] {}'.format(time.ticks_ms()/1000, len(nmea), nmea))
            if self._verify_checksum:
                try:
                    data, checksum = nmea.split('*')
                except Exception as e:
                    self._count_checksum_failed += 1
                    print('Failed to split nmea: ({}:{}) "{}"'.format(type(e), e, nmea))
                    continue
                calc_checksum = self._checksum(data[1:])
                if checksum != calc_checksum:
                    self._count_checksum_failed += 1
                    print('failed checksum "{}"'.format(nmea), '"{}"!="{}"'.format(checksum, calc_checksum))
                    continue
            self._count_checksum_ok += 1
            if self._nmea_parse_callback:
                self._verbose and print('read', nmea)
                self._nmea_parse_callback(nmea)
            else:
                print(nmea)
        print('stopped')
        self._running = False

    def stop(self):
        if not self._running:
            return
        print("stopping")
        self._stop = True
        for t in range(int(round(3+self._sleep_after_newline_s))):
            if not self._running:
                return
            time.sleep(1)
        print('timeout waiting for _running=False')

    def isrunning(self):
        return self._running

if __name__ == '__main__':
    import sys
    try:
        print('attempting to stop')
        gnss.stop()
        time.sleep(1)
    except Exception as e:
        print("cannot stop, that's ok")
        # sys.print_exception(e)

    print('init')
    gnss = Quectel_L76()#verbose=True)
    gnss._verify_checksum = True

    print('quiet')
    def filter(s):
        try:
            tokens = s.split('*')[0].split(',')
            cmd = tokens[0][3:]
            if cmd in [ 'VTG', 'GLL', 'GSV', 'GGA' ]:
                return
            if cmd == 'RMC':
                sec = int(float(tokens[1]))
                # if sec % 10 != 0:
                #     return
            print(tokens)
            # if tokens[0][3:] in [ 'RMC' ]:
            #     print(tokens)
        except Exception as e:
            print(e)
    # def echo(s):
    #     print()
    gnss.nmea_parse_callback(print)

    s = time.time()
    print('start', s)
    # gnss.run()
    import _thread
    _thread.start_new_thread(gnss.run, ())
    time.sleep(1)

    def w(msg):
        gnss.write_nmea(msg)

    if False:
        gnss.write_nmea('PMTK314,0,1,0,1,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0') # RMC,GGA,GSA,GSV,GRS
        print(gnss, gnss.isrunning())
        print('newline/zero/overflow', gnss._count_newline, gnss._count_zero, gnss._count_overflow)
        print(gnss._sleep_after_newline_s)
        gnss.stop()
        # reset/power
        gnss.write_nmea('PMTK101') # Hot start
        gnss.write_nmea('PMTK102') # Warm start
        gnss.write_nmea('PMTK103') # Cold start
        gnss.write_nmea('PMTK104') # Full cold start
        gnss.write_nmea('PMTK622,1') # dump LOCUS flash data.
        """
120 PMTK_CMD_CLEAR_FLASH_AID
161 PMTK_CMD_STANDBY_MODE
291 PMTK_CMD_BACKUP_MODE
225 PMTK_SET_PERIODIC _MODE
300 PMTK_API_SET_FIX_CTL rate of position fixing

# SBAS/dGPS
413 PMTK_API_Q_SBAS_ENABLED
gnss.write_nmea('PMTK413')
0=off, 1=on
313 PMTK_API_SET_SBAS_ENABLED
gnss.write_nmea('PMTK313,1')
PMTK_DT_DGPS_MODE no/rtcm/waas

401 PMTK_API_Q_DGPS_MODE
gnss.write_nmea('PMTK401')
0=no,1=rtcm,2=sbas
301 PMTK_API_SET_DGPS_MODE
$PMTK301,2

14 nmea output frequencies:
gnss.write_nmea('PMTK414')
0 disable
1-5, output every x fixes
                 PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
                         GLL VTG GSA GRS 0,0,0,0,0,0,0,0,0,ZDA
                           RMC GGA GSV GST                   MCHN
gnss.write_nmea('PMTK314,0,5,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,5,5')
gnss.write_nmea('PMTK314,5,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0') # GLL
gnss.write_nmea('PMTK314,0,5,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0') # VTG
gnss.write_nmea('PMTK314,0,5,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0') # GGA
gnss.write_nmea('PMTK314,0,5,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0') # GSA
gnss.write_nmea('PMTK314,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0') # GSA
gnss.write_nmea('PMTK314,0,5,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0') # GSV
gnss.write_nmea('PMTK314,0,5,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0') # GRS
gnss.write_nmea('PMTK314,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0') # GST
gnss.write_nmea('PMTK314,0,1,0,1,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0') # RMC,GGA,GSA,GSV,GRS

353 Constellation
$PMTK353,GPS,GLONASS,GALILEO,GALILEO_FULL,BEIDOU
gnss.write_nmea('PMTK353,1,1,0,0,0') # GPS GLONASS
gnss.write_nmea('PMTK353,1,1,1,0,0') # GPS GLONASS GALILEO      -> GSA:1,2,3
gnss.write_nmea('PMTK353,1,1,0,1,0') # GPS GLONASS GALILEO_FULL -> GSA:1,2
gnss.write_nmea('PMTK353,1,1,1,1,0') # GPS GLONASS GALILEO_FULL -> GSA:1,2,3
gnss.write_nmea('PMTK453') -> $PMTK001,453,1*30
2.12. PQJAM Jamming detection


w('PQEPE,W,0,0') # disable
w('PQEPE,W,1,0') # enable
        """


    if True:
        def ignore(s):
            pass
        gnss.nmea_parse_callback(ignore)

        def unzip():
            import uzlib
            import os
            # verify that the file has been uploaded
            file = 'test.txt.z'
            # print('dir', os.listdir())
            # print(file, os.stat(file))
            # read contents, decompress and print
            with open(file, 'r') as f:
                z = f.read()
            b = uzlib.decompress(z)
            s = b.decode('utf-8')
            print('ratio', len(z)/len(s))
            # print(s)

        def stress():
            print('stress')
            while True:
                unzip()
                time.sleep(2)

        _thread.start_new_thread(stress,())

        gnss._sleep_after_newline_s = 2
        gnss._verify_checksum = True
        print('_sleep_after_newline_s', gnss._sleep_after_newline_s)
        print('_verify_checksum', gnss._verify_checksum)
        t = time.time()
        run_time = 120
        # run_time = 600
        while (t-s) < run_time:
            t = time.time()
            ts = t-s
            nl = gnss._count_newline
            ok = gnss._count_checksum_ok
            fail = gnss._count_checksum_failed
            try:
                avg_newline_per_second = nl / ts
            except:
                avg_newline_per_second = None
            try:
                avg_checksum_ok_per_second = ok / ts
            except:
                avg_checksum_ok_per_second = None
            try:
                avg_checksum_fail_per_second = fail / ts
            except:
                avg_checksum_fail_per_second = None
            # print(t, 'newline/zero/overflow', n, gnss._count_zero, gnss._count_overflow, gnss._sleep_after_newline_s, gnss._count_checksum, avg_newline_per_second, avg_checksum_per_second)
            print(t, ts, gnss._count_zero, gnss._count_overflow, avg_newline_per_second, avg_checksum_ok_per_second, avg_checksum_fail_per_second, fail)
            time.sleep(2)

        gnss.stop()

"""
This implementation is polling the I2C bus, hence it is sensitive to CPU load. These is one main tuning parameter:
_sleep_after_newline_s
this determines how long we sleep when we encounter a (partially) empty buffer

Below are some measurements to evaluate the impact of this parameter on the number of correctly received nmea sentences
- set _sleep_after_newline_s and _verify_checksum
- safe boot Ctrl-F
- play this file
- record:
  - how long it ran [s],
  - _sleep_after_newline_s setting
  - _verify_checksum setting
  - avg_newline_per_second
  - avg_checksum_ok_per_second
  - avg_checksum_fail_per_second)

Results with the 'stress' label at the end are performed with an extra thread running stress() function doing unzip.

time sleep verify nl/s  ok/s fail/s
----|-----|------|-----|----|------
120    0      N   5317  12.3
120    0      Y   3236  13    0.008
120    0.1    Y   400   14.3  0
120    1      Y    42   13.2  0
120    1      Y    32   12.7  0.008
120    1      Y    34   12.8  0.008    stress
120    2      Y    28   13.2  0
120    2      N    34   13.2
120    2      Y    26   13.1  0        stress
600    2      Y    20.7 12.0  0.002(1) stress
120    4      Y    17   13    0.008
120    4      Y    16   12    0.017    stress
 90    5      N    19   14
120    5      Y    14   13.4
120    5      Y    16   11.4  0.008
120    5      Y    12   11    0.017    stress
120    7      Y    11.1 10.5  0.018
120   10      Y     7.1  7.8  0.032
120   20      Y     7.2  6.0  0.025

It seems the maximum number of sentences per second is 13-14 with default settings. So, we want to find parameters that give us this amount of messages - we don't want to miss sentences.

If we go to high sleep values of 7s and higher, we see checksum failures and an overall drop in nmea sentences per second to 10 and lower. This is not good. In order to not waste CPU resources by polling, we aim for a low nl/s value. With sleep=7 and checksum failures, we still see ca 11 nl/s, so no need to chase much lower values than that.

Generally sleep=5 seems to be a high value (low cpu pressure), but still gives rarely failed checksums (high accuracy). All cases of checksum failures I have seen were a single failure (fail/s=0.008) right after the start of script, which might be some other reason, and occasionally I also see this with very low sleep values.

When I run another background thread (stress) in the background, then sleep in [4,5] gives higher failed checksums.

So I'm going with 2s. This seems to be an ok compromise.
"""
