#!/usr/bin/env python3
# GNSS Tracker
# processes GNSS fixes and calculates distances and collects some stats

import time
from haversine import *

class Tracker:
    # last time we have tracked a fix
    s = None
    # ts = None
    # date = None
    lat = None
    # lon = None
    # snr = None
    # view = None
    # used = None
    total_km = 0

    def __init__(self, nmea_parser, track_file=None):
        self._nmea = nmea_parser
        self.start_s = time.time()
        self.ttff_s = None
        self.track_file = track_file
        self.verbose = False
        if self.track_file:
            with open(self.track_file, 'a') as f:
                f.write('s,DD,MM,YY,hh,mm,ss,Lat,Lon,km,total_km,used,GP,GL,GA,snr,msg')
                f.write('\n')

    def track(self, msg="", dist_km=None):
        lat = self._nmea._lat
        lon = self._nmea._lon
        if dist_km is None:
            if self.lat is None:
                self.km = None
            else:
                self.km = haversine(self.lat, self.lon, lat, lon)
        else:
            self.km = dist_km
        if self.km is not None:
            self.total_km += self.km
        self.s = time.time()
        self.ts = self._nmea._ts
        self.date = self._nmea._date
        self.lat = lat
        self.lon = lon
        self.alt = self._nmea._alt
        self.snr = self._nmea._snr
        self.used = self._nmea._sat_used
        self.view = self._nmea._sat_view
        msg = "{},{}/{}/20{} {}:{}:{},{},{},{},{},{},{},{},{},{},{}".format(self.s, self.date[1], self.date[0], self.date[2], self.ts[0], self.ts[1], self.ts[2], self.lat, self.lon, self.km, self.total_km, self.used, self.view.get('GP'), self.view.get('GL'), self.view.get('GA'), self.snr, msg)
        if self.verbose:
            print(msg)
        if self.track_file:
            with open(self.track_file, 'a') as f:
                f.write(msg)
                f.write('\n')

    def fix_callback(self):
        now_s = time.time()
        if self.s is None:
            # first fix
            self._nmea.print_stats(do_print=False) # needed to calc snr
            self.ttff_s = now_s - self.start_s
            self.track('TTFF={}'.format(self.ttff_s))
            return

        if now_s > self.s + 600:
            self.track('periodic')
            return

        if self.used != self._nmea._sat_used or self.view != self._nmea._sat_view:
            self.track('sats')
            return

        lat, lon = self._nmea.coords()
        dist_km = haversine(self.lat, self.lon, lat, lon)
        if dist_km > 0.001:
            self.track('dist', dist_km)
            return

        self._nmea.print_stats(do_print=False) # needed to calc snr
        snr = self._nmea._snr
        if snr is None:
            if self.snr is None:
                # nothing to do
                pass
            else:
                self.track('snr lost', dist_km)
                return
        else:
            if self.snr is None:
                self.track('snr', dist_km)
                return
            else:
                snr_thres = 3
                if snr > self.snr + snr_thres or snr + snr_thres < self.snr:
                    self.track('snr', dist_km)
                    return

        # todo dop

if __name__ == "__main__":
    try:
        print('try to run on pytrack')
        import pycom
        from NMEA import NMEA
        nmea = NMEA()

        tracker = Tracker(nmea)
        nmea.fix_callback(tracker.fix_callback)
        nmea.parse("$GPGGA,115212.000,5140.7559,N,00519.0310,E,1,6,3.32,17.0,M,47.3,M,,*6B")
        nmea.parse("$GNRMC,121312.000,A,5140.7663,N,00519.0269,E,0.95,249.60,051221,,,D,V*09")
        if True:
            nmea.parse("$GNRMC,124919.000,A,5140.7563,N,00519.0345,E,1.01,59.18,051221,,,D,V*31")
            nmea.parse("$GPGGA,125011.000,5140.7563,N,00519.0345,E,2,9,1.23,22.8,M,47.3,M,,*60")
            nmea.parse("$GNRMC,125012.000,A,5140.7563,N,00519.0345,E,0.00,94.46,051221,,,D,V*38")

        if False:
            from shell import *
            tail('/flash/track.log')
            cat('/flash/track.log')
            rm('/flash/track.log')
    except Exception as e:
        print(e)
        print('failed to run on pytrack, try laptop')
        import sys
        import os

        from NMEA import NMEA
        nmea = NMEA()
        # nmea._print_sat_stats = True

        track_file = 'track.csv'
        if len(sys.argv) > 1:
            file = sys.argv[1]
            print('read from file:', file)
            if not os.path.isfile(file):
                raise Exception('Cannot read file ("{}")'.format(file))
            track_file=file + '.csv'

        tracker = Tracker(nmea, track_file=track_file)
        nmea.print_stats_header()
        nmea.rmc_callback(nmea.print_stats)
        nmea.fix_callback(tracker.fix_callback)
        import fileinput
        for line in fileinput.input():
            # print(line)
            nmea.parse(line.strip())
