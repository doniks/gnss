import os
import binascii
import machine
import time

name = os.uname().sysname.lower() + '-' + binascii.hexlify(machine.unique_id()).decode("utf-8")[-4:]
log_base = "/flash/log_" + name + "_"
log_file = log_base + "0.log"
log_max = 2 # highest index of a log file on disk. files with log_max+1 and higher are removed

def log(*messages, quiet=False):
    # messages = ('eh', 'yo', 666, 'whazzzup!?')
    f = open(log_file, 'a')
    t = "[" + str(time.time()) + "]"
    # f.write(t)
    # f.write(' ')
    for m in messages:
        # print(m)
        f.write(str(m))
        f.write(' ')
    f.write('\n')
    f.close()
    # print(log_silent)
    if not quiet:
        print(t, *messages)

def log_cat():
    try:
        with open(log_file, 'r') as f:
            print('log_cat', log_file)
            # read in one go
            # print(f.read())
            # read line by line. this is more responsive with large files and can be Ctrl-C'ed
            for line in f:
                print(line, end='')
    except:
        pass

def log_tail(bytes=1000):
    try:
        l = os.stat(log_file)[6]
    except:
        return
    s = max(0, l-bytes)
    print('log_tail', log_file, s)
    with open(log_file, 'r') as f:
        f.seek(s)
        print(f.read())

def log_size():
    for l in range(log_max+1):
        f = log_base + str(l) + ".log"
        try:
            s = os.stat(f)
            size = s[6]
            # print(size)
            print("{:<40} ".format(f), end='')
            print('{:>9,}'.format(size), 'B', end='')
            if size > 1024:
                print(' = {:>7,.1f}'.format(round(size/1024,2)), ' KiB', sep='', end='')
                if size > 1024 * 1204:
                    print(' = {:>2.1f}'.format(round(size / 1024 / 1024,2)), ' MiB', sep='', end='')
            print()
        except:
            pass
    free_kib = os.getfree('/flash')
    free_b = free_kib * 1024
    # free_mib = free_kib / 1024
    print('Free:', free_kib * 1024, 'B free ({}'.format(free_kib), "KiB)")

def _log_rm(f):
    print('log_rm', f)
    try:
        os.remove(f)
        print("done")
    except Exception as e:
        print('couldn\'t remove "{}" {}:{}'.format(f, type(e), e))

def log_rm(all=False):
    if all:
        for l in range(log_max+1):
            f = log_base + str(l) + ".log"
            try:
                os.stat(f)
                _log_rm(f)
            except:
                pass
    else:
        try:
            os.stat(log_file)
            _log_rm(log_file)
        except:
            pass

def log_rotate(bytes=200_000):
    try:
        s = os.stat(log_file)[6]
    except:
        print('log_rotate: "{}" doesn\'t exist. nothing to rotate'.format(log_file))
        return
    if s <= bytes:
        print('log_rotate: "{}"'.format(log_file), s, '<=', bytes, 'nothing to do')
        return
    print("log_rotate:", log_file, s, '>', bytes, log_base, log_max)
    # remove the oldest file
    f = log_base + str(log_max) + ".log"
    try:
        os.remove(f)
        print("remove", f)
    except Exception as e:
        print("cannot remove:", f, e)
        pass
    # shift all existing files up one index
    for l in range(log_max,0,-1):
        old = log_base + str(l-1) + ".log"
        new = log_base + str(l) + ".log"
        try:
            # mv(old, new)
            os.rename(old, new)
            print("rename", l-1, l, old, new)
        except Exception as e:
            print("cannot rename:", old, e)
            pass

if __name__ == "__main__":
    # log('eh', 'yo', 666, 'whazzzup!?')
    # print('---')
    # log('secret', quiet=False)
    log_tail()
    # log_cat()
    log_size()

    if False:
        log_rotate()
        log_cat()
        log_rm()
        log_rm(all=True)
