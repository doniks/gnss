PSTM_PAR = {}

# Application ON/OFF
# Allow enabling/disabling different features in the GNSS library.

# CDB-ID 200 represents the first 32 bits (low 32 bits)
PSTM_PAR[200] = [
'Application on/off low bits',
[
'RESERVED',
'RESERVED',
'SBAS (WAAS / EGNOS)',
'SBAS reporting in GSV',
'STAGP',
'2.5ppm TCXO support',
'RESERVED',
'QZSS distributed acquisition',
'UNDOCUMENTED',
'RESERVED',
'RESERVED',
'RTCM',
'FDE False Detection and Exclusion',
'UNDOCUMENTED',
'Walking Mode Algorithm',
'Stop Detection Algorithm',
'GPS constellation',
'GLONASS constellation',
'QZSS constellation',
'GNGSV "GN" talker ID',
'GNGSA "GN" talker ID',
'GLONASS usage for positioning',
'GPS usage for positioning',
'QZSS usage for positioning',
'PPS',
'PPS polarity inversion',
'Position Hold',
'TRAIM algorithm',
'RESERVED',
'RESERVED',
'RESERVED',
'Low power algorithm',
] ]
assert len(PSTM_PAR[200][1]) == 32


# CDB-227 represents the second 32 bits (high 32 bits)
PSTM_PAR[227] = [
'Application on/off high bits',
[
'UNDOCUMENTED',
'commands eco',
'Time To First Fix message',
'Few satellites position estimation',
'RESERVED',
'Return Link Message',
'RESERVED',
'Galileo constellation enable',
'Galileo usage for positioning',
'BEIDOU constellation',
'BEIDOU usage for positioning',
'RESERVED',
'RTC usage disabling',
'Fast Satellite Drop feature',
'RESERVED',
'Excluded satellites reporting',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RTC calibration enable',
'UNDOCUMENTED',
'UNDOCUMENTED',
'UNDOCUMENTED',
'UNDOCUMENTED',
] ]
assert len(PSTM_PAR[227][1]) == 32



# CDB-ID 190 - CDB-ID 201 - CDB-ID 228 -
# NMEA on UART message list parameters

# CDB-ID 201 and CDB-ID 228 allow enabling/disabling each NMEA message in the message list 0

# CDB-ID 190 allows setting the message list output rate for the message list 0. It is a scaling factor referred to the selected fix rate. The default value is 1 and this means that the messages are sent out on every fix. Setting the scaling factor to "N" means that the corresponding message list is sent out every "N" fixes.




# CDB-ID 201 represents the first 32 bits (low bits) of the extended 64 bits NMEA message list
PSTM_PAR[201] = [
'Message list low bits',
[
'$GPGNS',
'$GPGGA',
'$GPGSA',
'$GPGST',
'$GPVTG',
'$PSTMNOISE',
'$GPRMC',
'$PSTMRF',
'$PSTMTG',
'$PSTMTS',
'$PSTMPA',
'$PSTMSAT',
'$PSTMRES',
'$PSTMTIM',
'$PSTMWAAS',
'$PSTMDIFF',
'$PSTMCORR',
'$PSTMSBAS',
'$PSTMTESTRF',
'$GPGSV',
'$GPGLL',
'$PSTMPPSDATA',
'RESERVED',
'$PSTMCPU',
'$GPZDA',
'$PSTMTRAIMSTATUS',
'$PSTMPOSHOLD',
'$PSTMKFCOV',
'$PSTMAGPS',
'$PSTMLOWPOWERDATA',
'$PSTMNOTCHSTATUS',
'$PSTMTM',
] ]
assert len(PSTM_PAR[201][1]) == 32

# hex2flags.py 0x00980056
# 0x980056 0b100110000000000001010110 9961558:
#        2 bit:  1 gns
#        4 bit:  2 gga
#       10 bit:  4 gsa
#       40 bit:  6 rmc
#    80000 bit: 19 gsv
#   100000 bit: 20 gll
#   800000 bit: 23 cpu

# $PSTMSETPAR,1201,0x20000,1

# CDB-ID 228 for the second 32 bits (high bits) of the 64 bits message list
PSTM_PAR[228] = [
'Message list high bits',
[
'$PSTMPV',
'$PSTMPVQ',
'$PSTMUTC',
'$PSTMADCDATA',
'RESERVED',
'RESERVED',
'$PSTMUSEDSATS',
'$GPDTM',
'$PSTMEPHEM',
'$PSTMALMANAC',
'$PSTMIONOPARAMS',
'RESERVED',
'$PSTMBIASDATA',
'$GPGBS',
'$PSTMPVRAW',
'RESERVED',
'$PSTMFEDATA',
'RESERVED',
'$PSTMODO',
'$PSTMGEOFENCESTATUS',
'$PSTMLOGSTATUS',
'$PSTMGNSSINTEGRITY',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'RESERVED',
'$--RLM',
] ]
assert len(PSTM_PAR[228][1]) == 32

def print_pstm_par(par, val, verbose=True):
    doc = PSTM_PAR.get(int(par), None)
    if doc:
        print('{} {:8x}'.format(par, val), doc[0])
        for i in range(len(doc[1])):
            s = ' {: >2d} {:8x} {}'.format(i, 1<<i, doc[1][i])
            if val & 1 << i:
                if verbose:
                    print(' +', s)
                else:
                    print(s)
            else:
                if verbose:
                    print(' -', s)


    else:
        print('{} {:8x}'.format(par, val))

if __name__ == '__main__':
    # $PSTMSETPAR,1200,0x19639a5c*53
    par = 200
    # par = 228
    val = 0x19639a5c
    print_pstm_par(par, val)


    # constexpr int32_t MSG_LIST_LOW_CONFIGURATION_IDENTIFIER = 1201;
    # constexpr int32_t SET_MSG_LIST_LOW_CONFIGURATION_IDENTIFIER = 3201;
    # constexpr char MESSAGE_LIST_LOW_CONFIGURATION[] = "0x0008004e";
    print_pstm_par(201, 0x0008004e)

    # constexpr int32_t MSG_LIST_HIGH_CONFIGURATION_IDENTIFIER = 1228;
    # constexpr int32_t SET_MSG_LIST_HIGH_CONFIGURATION_IDENTIFIER = 3228;
    # constexpr char MESSAGE_LIST_HIGH_CONFIGURATION[] = "0x00000004";
    print_pstm_par(228, 0x00000004)

    # constexpr int32_t APPLICATION_ON_OFF_1_CONFIGURATION_IDENTIFIER = 1200;

    # constexpr int32_t APPLICATION_ON_OFF_2_CONFIGURATION_IDENTIFIER = 1227;
    # constexpr int32_t SET_APPLICATION_ON_OFF_2_CONFIGURATION_IDENTIFIER = 3227;
    # constexpr char APPLICATION_ON_OFF_2_CONFIGURATION[] = "0x040000cc";
    print_pstm_par(227, 0x040000cc)
    # $PSTMSETPAR,1227,040000cc

