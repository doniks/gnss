#!/bin/bash

set -xev
test -n "$1"
test -r "$1"
in=$1

bin=$(basename $0)
echo $bin

case $bin in
    "nmea2gpx")
        fmt=gpx
        out=$in.gpx
        ;;
    "nmea2kml")
        fmt=kml
        out=$in.kml
        ;;
    *)
        exit 1
        ;;
esac
echo $out


gpsbabel -i nmea -f $in -o $fmt -F $out
# cat log_fipy-0220_0.log | sed -e 's/^.[0-9]*. //' | gpsbabel -i nmea -f - -o gpx -F out.gpx
ls -ltr $in $out
