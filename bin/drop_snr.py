#!/usr/bin/env python3
import re
import sys
import code

# def interact():
#     import code
#     code.InteractiveConsole(locals=globals()).interact()

def process():
    global file
    while True:
        line = file.readline()
        if not line:
            break
        line = line.strip()

        match = re.search('^\d?\d:\d\d:\d\d( \d\d)*$', line)
        if match:
            # print('skip header', line)
            continue

        match = re.search('--\|', line)
        if match:
            # print('skip dashes', line)
            continue

        match = re.search('^\d?\d:\d\d:\d\d (G[A-Z] *)*$', line)
        if match:
            # print('skip header', line)
            continue

        match = re.search('^\d?\d:\d\d:00 ', line)
        if match is None:
            # print('drop non-zero second', line)
            continue

        line = re.sub(" (\d?\d +)+"," ",line.strip())
        line = re.sub(", +", ",", line)
        line = re.sub(" +", ",", line)
        line = re.sub("[/{}]", ",", line)
        print(line)

print('time,snr,fix,sats,,,gp,,gl,,pdop,hdop,vdop,lat,lon,alt')
if len(sys.argv) > 1:
    filename = sys.argv[1]
    file = open(filename)
    if file:
        process()
    file.close()
else:
    file = sys.stdin
    process()
